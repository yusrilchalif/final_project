﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Ballmanager : NetworkBehaviour
{
    public GameObject bolaPrefabs;
    bool isShowObject = false;
    GameObject bola;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!isServer)
        {
            return;
        }
        
        //ketika terdapat dua player maka permainan akan dimulai
		if (NetworkServer.connections.Count == 2) {
			//memastikan bola belom dibuat sebelomnya
			if (!isShowObject) {
				//kemudian bola diciptakan di server yang nanti diteruskan ke client
				bola = (GameObject)Instantiate (bolaPrefabs);
				NetworkServer.Spawn (bola);
				isShowObject = true;
			}
		} else {
			if (isShowObject) {
				//bola dihapus
				Destroy (bola);
				isShowObject = false;
			}
		}
    }
}
