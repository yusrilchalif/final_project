﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{
    public int ballSpeed;
    Rigidbody2D rb;
    public int scoreP1;
    public int scoreP2;
    public Text scoreUIP1;
    public Text scoreUIP2;
    public GameObject panel;
    Text WinText;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Vector2 dir = new Vector2(0, 2).normalized;
        rb.AddForce(dir * ballSpeed);
        scoreUIP1 = GameObject.Find("TextP1").GetComponent<Text>();
        scoreUIP2 = GameObject.Find("TextP2").GetComponent<Text>();
        panel = GameObject.Find("Panel");
        scoreP1 = 0;
        scoreP2 = 0;
        panel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if(other.gameObject.name == "batas_atas")
        {
            scoreP1 += 1;
            ShowScore();
            if(scoreP1 == 10)
            {
                panel.SetActive(true);
                WinText = GameObject.Find("WinText").GetComponent<Text>();
                WinText.text = "Player 1 win";
                Destroy(gameObject);
                return;
            }
            ResetBall();
            Vector2 dir = new Vector2(0, 2).normalized;
            rb.AddForce(dir * ballSpeed);
        }

        if(other.gameObject.name == "batas_bawah")
        {
            scoreP2 += 1;
            ShowScore();
            if(scoreP2 == 10)
            {
                panel.SetActive(true);
                WinText = GameObject.Find("WinText").GetComponent<Text>();
                WinText.text = "Player 2 win";
                Destroy(gameObject);
                return;
            }
            ResetBall();
            Vector2 dir = new Vector2(0, -2).normalized;
            rb.AddForce(dir * ballSpeed);
        }

        if(other.gameObject.name == "padlle 1" || other.gameObject.name == "padlle2")
        {
            float corner = (transform.position.y - other.transform.position.y) * 5f;
            Vector2 dir = new Vector2(rb.velocity.x, corner).normalized;
            rb.velocity = new Vector2(0,0);
            rb.AddForce(dir * ballSpeed *2);
        }
    }

    void ResetBall()
    {
        transform.localPosition = new Vector2 (0,0);
        rb.velocity = new Vector2 (0,0);
    }

    void ShowScore()
    {
        scoreUIP1.text = scoreP1 + "";
        scoreUIP2.text = scoreP2 + "";
    }
}
