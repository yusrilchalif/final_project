﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Network : MonoBehaviour
{
    public int portNumber = 77777;
    public string IPadress ="localhost";
    public GameObject panel;
    NetworkManager network;
    bool isNetworkActive = false;

    void Start()
    {
        network = GameObject.Find("Network Manager").GetComponent<NetworkManager>();
    }

    void Update()
    {
        if(!ClientScene.ready || NetworkServer.active)
        {
            if(!isNetworkActive){
                panel.SetActive(false);
                isNetworkActive = true;
            }
        }else{
            if(isNetworkActive){
                panel.SetActive(true);
                isNetworkActive =false;
            }
        }
    }

    public void StartHost(){
        if(!NetworkServer.active){
            if(NetworkClient.active){
                network.StopClient();
            }
            network.networkPort = portNumber;
            network.networkAddress = IPadress;
            network.StartHost();
        }
    }

    public void StartJoinGame(){
        if(!NetworkClient.active && !NetworkServer.active){
            network.networkPort = portNumber;
            network.networkAddress = IPadress;
            network.StartClient();
        }
    }
}