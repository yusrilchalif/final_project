﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MoveAccelo : MonoBehaviour
{
    Rigidbody2D rb;
    float dirX;
    float MoveSpeed = 15f;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!isLocalPlayer){
            return;
        }

        dirX = Input.acceleration.x * MoveSpeed;
        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -2.0f, 2.0f), transform.position.y);
        Debug.Log("bergerak");
    }

    void FixedUpdate() {
        rb.velocity = new Vector2(dirX, 0f);
    }
}
