﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;

public class PlayAuth : MonoBehaviour
{
    public static PlayGamesPlatform platform;
    public string leaderboard;
    public Button login;
    public Button logout;

    void Start()
    {
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        logout.gameObject.SetActive(false);
        login.gameObject.SetActive(true);
    }

    public void SignIn()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if(success)
            {
                Debug.Log("Success");
                logout.gameObject.SetActive(true);
                // login.gameObject.SetActive(true);
            }
            else
            {
                Debug.Log("Failled");
                logout.gameObject.SetActive(true);
                // login.gameObject.SetActive(true);
            }
        });

    }

    public void Logout()
    {
        PlayGamesPlatform.Instance.SignOut();
        logout.gameObject.SetActive(false);
        login.gameObject.SetActive(true);
    }

    // public void CreateQuickGame()
    // {
    //     const int MinJoin = 1, MaxJoin = 2;
    //     const int minInvite = 1, maxInvite = 2;
    //     const int GameVariant = 0;

    //     PlayGamesPlatform.Instance.RealTime.CreateQuickGame(MinJoin, MaxJoin, GameVariant, this);
    //     PlayGamesPlatform.Instance.RealTime.CreateWithInvitationScreen(minInvite, maxInvite, GameVariant, this);
    // }

    // #region RealTimeMultiplayerListener implementation
	// private bool isRoomSetup = false;
    // void RealTimeMultiplayerListener.OnRoomSetupProgress(float percent)
    // {
    //     //throw new System.NotImplementedException();
	// 	if(percent >= 20){
	// 		isRoomSetup = true;
	// 		Debug.Log("Finding Match");
	// 		PlayGamesPlatform.Instance.RealTime.ShowWaitingRoomUI();
	// 	}
    // }
    // private bool isConnected = false;
    // void RealTimeMultiplayerListener.OnRoomConnected(bool success)
    // {
    //     //throw new System.NotImplementedException();
	// 	if(success){
	// 		//instance your player on network
	// 		isConnected = true;

	// 		// player = Instantiate(prefab, new Vector3(0f, 4f, 0f), Quaternion.identity);
	// 		// player.name = PlayGamesPlatform.Instance.RealTime.GetSelf().ParticipantId;

	// 		bool reliability = true;
	// 		string data = "Instantiate:0:1:2";
	// 		byte[] bytedata = System.Text.ASCIIEncoding.Default.GetBytes(data); 
	// 		PlayGamesPlatform.Instance.RealTime.SendMessageToAll(reliability, bytedata);
    //         // Application.LoadLevel("Game");
	// 	}else{
	// 		isConnected = false;
	// 		CreateQuickGame();
    //         Application.LoadLevel("Game");
	// 	}
    // }

    // void RealTimeMultiplayerListener.OnLeftRoom()
    // {
    // 	//throw new System.NotImplementedException();
	// 	isConnected = false;
    // }

    // void RealTimeMultiplayerListener.OnParticipantLeft(Participant participant)
    // {
    //     //throw new System.NotImplementedException();
    // }

    // void RealTimeMultiplayerListener.OnPeersConnected(string[] participantIds)
    // {
    //     //throw new System.NotImplementedException();
    // }

    // void RealTimeMultiplayerListener.OnPeersDisconnected(string[] participantIds)
    // {
    //     //throw new System.NotImplementedException();
    // }

    // void RealTimeMultiplayerListener.OnRealTimeMessageReceived(bool isReliable, string senderId, byte[] data)
    // {
    //     //throw new System.NotImplementedException();
	// 	// if(!PlayGamesPlatform.Instance.RealTime.GetSelf().ParticipantId.Equals(senderId)){
	// 	// 	string rawdata = System.Text.ASCIIEncoding.Default.GetString(data);
	// 	// 	string[] sliced = rawdata.Split(new string[1] {":"}, System.StringSplitOptions.RemoveEmptyEntries);

	// 	// 	if(sliced[0].Contains("Instantiate")){
	// 	// 		Transform naming = Instantiate(prefab, new Vector3(0f, 4f, 0f), Quaternion.identity);
	// 	// 		naming.name = senderId;

	// 	// 		naming.GetChild(0).gameObject.SetActive(false);
	// 	// 	}else if(sliced[0].Contains("Position")){							//change data var to sliced
	// 	// 		Transform target = GameObject.Find(senderId).transform;
	// 	// 		if(target == null){
	// 	// 			return;
	// 	// 		}
	// 	// 		Vector3 newpos = new Vector3
	// 	// 		(
	// 	// 			System.Convert.ToSingle(data[1]), 						//change data var to sliced
	// 	// 			System.Convert.ToSingle(data[2]),						//change data var to sliced
	// 	// 			System.Convert.ToSingle(data[3])						//change data var to sliced
	// 	// 		);
	// 	// 			target.position = Vector3.Lerp(target.position, newpos, Time.deltaTime * 3.0f);
	// 	// 		}
	// 	// 	}
	// 	#endregion
	// }

    public void Leaderboard()
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI(leaderboard);
    }
}