﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pemukul : MonoBehaviour
{
    public float batasAtas;
    public float batasBawah;
    public float kecepatan;
    public string axis;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float gerak = Input.acceleration.x * kecepatan * Time.deltaTime;
        float NextPos = transform.position.x  + gerak;

        if(NextPos > batasAtas)
        {
            gerak = 0;
        }
        if(NextPos < batasBawah)
        {
            gerak = 0;
        }
        transform.Translate(0, gerak, 0);
    }
}
